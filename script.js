// define data
const data = {
  title: "Halo! I'm Rocky",
  subTitle: "My name is Rayhan Iqbal, I graduated from Universitas Nusamandiri with Informatics Engineering. Currently i'm learning about Software Development, from the designing and developing aspects.",
  card: [
    {
      title: "GitLab",
      subTitle: "GitLab Inc. is an open-core company that provides GitLab, a DevOps software package that combines the ability to develop, secure, and operate software in a single application. The open source software project was created by Ukrainian developer Dmitriy Zaporozhets and Dutch developer Sytse Sijbrandij."
    },
    {
      title: "HTML/CSS",
      subTitle: "Cascading Style Sheets is a style sheet language used for describing the presentation of a document written in a markup language such as HTML or XML. CSS is a cornerstone technology of the World Wide Web, alongside HTML and JavaScript"
    },
    {
      title: "Bootstrap",
      subTitle: "Bootstrap is a free and open-source CSS framework directed at responsive, mobile-first front-end web development. It contains HTML, CSS and JavaScript-based design templates for typography, forms, buttons, navigation, and other interface components"
    },
  ]
}

// define element HTML
const elementHtml = {
  title: document.getElementById("headingTitle"),
  subTitle: document.getElementById("headingSubTitle"),
  card: document.getElementById("cardGroup")
}

// implementation
elementHtml.title.innerHTML = data.title;
elementHtml.subTitle.innerHTML = data.subTitle;
// for(let i = 0; i < 3; i++) {
//   elementHtml.card.innerHTML += `
//     <div class="card" style="width: 20rem; height:22rem;">
//       <div class="card-body">
//         <h5 id="itemName" class="card-title">${data.card[i].title}</h5>
//         <p id="itemDesc" class="card-text">${data.card[i].subTitle}</p>
//         <a href="#" class="btn btn-secondary" id="button">View Details>></a>
//       </div>
//     </div>
//   `;
// }
data.card.map((item) => {
  elementHtml.card.innerHTML += `
    <div class="card" style="width: 20rem; height:22rem;">
      <div class="card-body">
        <h5 id="itemName" class="card-title">${item.title}</h5>
        <p id="itemDesc" class="card-text">${item.subTitle}</p>
        <a href="#" class="btn btn-secondary" id="button">View Details>></a>
      </div>
    </div>
  `;
})

function onChangePage() {
  location.href="./tugas.html";
}

